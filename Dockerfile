FROM python:3.11.4

WORKDIR /code

RUN pip3 install poetry

COPY ./pyproject.toml /code/pyproject.toml
COPY ./poetry.lock /code/poetry.lock

RUN poetry config virtualenvs.create false
RUN poetry install

COPY ./shortie /code/shortie

CMD ["poetry", "run", "uvicorn", "shortie.app:app", "--host", "0.0.0.0", "--port", "80"]