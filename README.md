# shortie

## Description
https://codingchallenges.fyi/challenges/challenge-url-shortener

This challenge is to build your own URL shortening service. Think bit.ly or tinyurl.com.

Basically it’s a service that lets a client submit a long URL which is then shortened to make it easier to use. For example:

https://www.amazon.com/Rust-Programming-Language-2nd/dp/1718503105/ref=sr_1_1?crid=3977W67XGQPJR&keywords=the+rust+programming+language&qid=1685542718&sprefix=the+%2Caps%2C3079&sr=8-1

could become: https://tinyurl.com/bdds8utd

## Installation
This project can be run using poetry or docker.

### Poetry
Keep in mind that you will need to provide a reachable redis url at environment variable REDIS_OM_URL (localhost:6379 by default) pointing to a installation that supports RediSearch.
```bash
poetry install
poetry run uvicorn shortie.app:app --port <your prefered port>
```

### Docker
```bash
docker-compose up --build
```

## Usage
Check auto-generated OpenAPI documentation at endpoint `/docs`