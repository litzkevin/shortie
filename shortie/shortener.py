import hashlib

from redis_om.model.model import NotFoundError
from pydantic import HttpUrl

from loguru import logger

from shortie.model import ShortUrl

def shorten(url: HttpUrl) -> ShortUrl:
    str_url = str(url)
    url_pair = find_by_url(str_url)
    if url_pair:
        logger.info("URL already shortened, reutilizing existing pair")
        return url_pair

    hex = hashlib.sha256(str_url.encode()).hexdigest()
    code = f'{hex[:4]}{hex[-4:]}'

    url_pair = ShortUrl(url=str_url, code=code)
    url_pair.save()

    return url_pair

def delete_shortened(code: str):
    short_url = find_by_code(code)
    if short_url:
        ShortUrl.delete(short_url.pk)
    return

def list_shortened() -> list[ShortUrl] | None:
    return [ ShortUrl.get(pk) for pk in ShortUrl.all_pks() ]

def find_by_url(url: str) -> ShortUrl | None:
    try:
        short_url = ShortUrl.find(ShortUrl.url == url).first()
        return short_url # type: ignore
    except NotFoundError:
        return None

def find_by_code(code: str) -> ShortUrl | None:
    try:
        query = ShortUrl.find(ShortUrl.code == code)
        short_url = query.first()
        return short_url # type: ignore
    except NotFoundError:
        logger.info(f'Code "{code}" not found.')
        return None