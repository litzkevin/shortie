from typing import Annotated

from fastapi import Body, FastAPI
from fastapi.responses import RedirectResponse, JSONResponse

from pydantic import HttpUrl

from loguru import logger

from shortie import shortener

app = FastAPI()

@app.post("/url")
def shorten_url(
    url: Annotated[HttpUrl, Body(embed=True)]
):
    logger.info(f'Shortening URL: {url}')
    short_url = shortener.shorten(url)
    return JSONResponse(content=short_url.dict(), status_code=201)

@app.get("/url")
def list_short_urls():
    logger.info('Listing URLs')
    short_urls = shortener.list_shortened()
    if not short_urls:
        return JSONResponse(content="No URL(s) found to list", status_code=404) 
    return JSONResponse(content=short_urls, status_code=200)

@app.get("/{code}")
def read_shorten_url_code(
    code: str
):
    short_url = shortener.find_by_code(code)
    if not short_url:
        return JSONResponse(content="URL not found", status_code=404) 
    return RedirectResponse(url=str(short_url.url), status_code=302)

@app.delete("/{code}")
def delete_shorten_url_code(
    code: str
):
    shortener.delete_shortened(code)
    return JSONResponse(content=None, status_code=200)