from pydantic.v1 import HttpUrl

from redis_om import HashModel, Field, Migrator

class ShortUrl(HashModel):
    url: HttpUrl = Field(index=True)
    code: str = Field(index=True)

Migrator().run()